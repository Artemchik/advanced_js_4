fetch(`https://ajax.test-danit.com/api/swapi/films`)
    .then(response => response.json())
    
fetch("https://ajax.test-danit.com/api/swapi/films")
    .then(response => response.json())
    .then(data => {
        data.forEach(({id,episodeId,name,openingCrawl,characters}) => {
            new Episode(id,episodeId,name,openingCrawl).render();
            new CharacterPull(id,characters).render();
        })
    })

class Episode {
    constructor (id,episodeId, name, opening) {
        this.name = name;
        this.id = id;
        this.episodeId = episodeId;
        this.opening = opening;
    }
    render(){
        const containerById = document.querySelector(`#episode_${this.id}`);

        const episodeDesc = `
            <h4>${this.name}</h4>
            <p>Episode: ${this.episodeId}</p>
            <p>Episode name: "${this.name}"</p>
            <p>Short introduction: ${"  " + this.opening}</p>
        `

        containerById.insertAdjacentHTML("beforeend", episodeDesc);
    }
}
class CharacterPull {
    constructor (id,characters) {
        this.id = id;
        this.characters = characters;
    }
    render(){
        const containerById = document.querySelector(`#episode_${this.id}`);

        this.characters.forEach(elem => {
            fetch(elem).then(response => response.json()).then(charData =>{
                const characterShow =`<p>${charData.name}</p>`

                containerById.insertAdjacentHTML("beforeend", characterShow);

            } )
        })
    }
}



